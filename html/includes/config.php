<?php
function config($key) {
  $config = [
    "DB_SERVER" => "mysql",
    "DB_USERNAME" => "testuser",
    "DB_PASSWORD" => "testpassword",
    "DB_NAME" => "webdb",
    "nav_items" => [
      "Index" => "/",
      "Post" => "/post",
      "Sign Up" => "/signup",
      "Login" => "/login",
      "Logout" => "/logout"
    ],
  ];
  return $config[$key];
}
?>
