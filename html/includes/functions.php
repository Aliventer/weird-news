<?php
require_once "config.php";

$conn = new mysqli(config("DB_SERVER"), config("DB_USERNAME"), config("DB_PASSWORD"), config("DB_NAME"));

if($conn -> connect_error) {
    die("Connection failed: " . $conn -> connect_error);
}

function nav_menu() {
    $nav_menu = "<nav class=\"navbar navbar-expand-lg navbar-light bg-light\">";
    foreach (config("nav_items") as $name => $uri) {
        $is_active = $_SERVER["REQUEST_URI"] == $uri ? " active" : "";
        $nav_menu .= "<a href=\"" . $uri . "\" title=\"" . $name . "\" class=\"nav-item nav-link" . $is_active . "\">" . $name . "</a>";
    }
    $nav_menu .= "</nav>";
    echo $nav_menu;
}
