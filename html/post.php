<?php
require_once "includes/functions.php";

session_start();
if (!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] === false) {
    header("location: /login");
    exit;
}

$content_err = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
  $content = $_POST["content"];
  if (strlen($content) < 255) {
    $query = "INSERT INTO posts (poster, content) VALUES (?, ?)";
    if ($stmt = $conn -> prepare($query)) {
      $stmt -> bind_param("ss", $param_poster, $param_content);

      $param_poster = $_SESSION["username"];
      $param_content = htmlspecialchars($content);

      if ($stmt -> execute()) {
        header("location: /");
      }
    }
    $stmt -> close();
  } else {
    $content_err = "Your post should be less than 255 characters long.";
  }
}
?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Weird News</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <style>
    .content-form {
      width: 390px;
      margin: 30px auto;
    }
    </style>
  </head>
  <body>
    <?php nav_menu(); ?>
    <div class="content-form">
      <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
        <h2>Post</h2>
        <div class="form-group">
          <textarea class="form-control" name="content" rows="9" placeholder="Write something here..." required></textarea>
          <span class="help-block"><?php echo $content_err; ?></span>
        </div>
        <div class="form-group">
          <button type="submit" class="btn btn-outline-primary btn-lg">Submit</button>
        </div>
      </form>
    </div>
  </body>
</html>
