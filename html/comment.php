<?php
require_once "includes/functions.php";

session_start();
if (!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] === false) {
    header("location: /login");
    exit;
}

$content_err = "";

if ($_SERVER["REQUEST_METHOD"] == "GET") {
  $id = $_GET["id"];
  $query = "SELECT username, comment FROM comments WHERE post_id = ? ORDER BY created_at DESC";
  if ($stmt = $conn -> prepare($query)) {
    $stmt -> bind_param("i", $param_id);
    $param_id = $id;

    if ($stmt -> execute()) {
      $stmt -> bind_result($username, $comment);
      $inner = "<hr>";
      while ($stmt -> fetch()) {
        $inner .= "<textarea class=\"form-control\" name=\"content\" rows=\"3\" readonly>" . $comment . "</textarea>";
        $inner .= "<span class=\"help-block\">" . "By: " . $username . ".";
      }
    }
    $stmt -> close();
  }
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
  $content = $_POST["content"];
  $id = $_POST["id"];
  if (strlen($content) < 255) {
    $query = "INSERT INTO comments (post_id, username, comment) VALUES (?, ?, ?)";
    if ($stmt = $conn -> prepare($query)) {
      $stmt -> bind_param("iss", $param_id, $param_poster, $param_content);

      $param_id = $id;
      $param_poster = $_SESSION["username"];
      $param_content = htmlspecialchars($content);

      if ($stmt -> execute()) {
        header("location: /comment?id=$id");
      }
      $stmt -> close();
    }
  } else {
    $content_err = "Your comment should be less than 255 characters long.";
  }
}
?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Weird News</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <style>
    .content-form {
      width: 390px;
      margin: 30px auto;
    }
    </style>
  </head>
  <body>
    <?php nav_menu(); ?>
    <div class="content-form">
      <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
        <h2>Comment</h2>
        <div class="form-group">
          <textarea class="form-control" name="content" rows="3" placeholder="Write something here..." required></textarea>
          <input type="hidden" name="id" value=<?php echo $id; ?>>
          <span class="help-block"><?php echo $content_err; ?></span>
        </div>
        <div class="form-group">
          <button type="submit" class="btn btn-outline-primary btn-lg">Submit</button>
        </div>
      </form>
      <div class="form-group">
        <?php echo $inner; ?>
      </div>
    </div>
  </body>
</html>
