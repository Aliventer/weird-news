<?php
require_once "includes/functions.php";

session_start();
if (isset($_SESSION["loggedin"]) && $_SESSION["loggedin"] === true) {
    header("location: /");
    exit;
}

$username = htmlspecialchars(trim($_POST["username"]));
$password = trim($_POST["password"]);
$username_err = $password_err = $confirm_password_err = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {

  $query = "SELECT id FROM users WHERE username = ?";
  if ($stmt = $conn -> prepare($query)) {
    $stmt -> bind_param("s", $param_username);

    $param_username = $username;

    if ($stmt -> execute()) {
      $stmt -> store_result();
      if ($stmt -> num_rows == 1) {
        $username_err = "This username is already taken.";
      } else if (strlen($username) > 50) {
        $username_err = "Username cannot contain more than 50 characters.";
      }
    }
    $stmt -> close();
  }

  if (strlen(trim($_POST["password"])) < 8) {
    $password_err = "Password must be at least 8 characters long.";
  } else if (strlen($password) > 255) {
    $password_err = "Password cannot contain more than 255 characters.";
  }

  $confirm_password = trim($_POST["confirm_password"]);
  if (empty($password_err) && ($password != $confirm_password)) {
    $confirm_password_err = "Password did not match.";
  }

  if (empty($username_err) && empty($password_err) && empty($confirm_password_err)) {
    $query = "INSERT INTO users (username, password, admin) VALUES (?, ?, 0)";
    if ($stmt = $conn -> prepare($query)) {
      $stmt -> bind_param("ss", $param_username, $param_password);

      $param_username = $username;
      $param_password = password_hash($password, PASSWORD_DEFAULT);

      if ($stmt -> execute()) {
        session_start();
        $_SESSION["loggedin"] = true;
        $_SESSION["id"] = $id;
        $_SESSION["username"] = $username;

        header("location: /");
      }
      $stmt -> close();
    }
  }
}
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <title>Weird News</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <style>
      .signup-form {
        width: 390px;
        margin: 30px auto;
      }
    </style>
  </head>
  <body>
    <?php nav_menu(); ?>
    <div class="signup-form">
      <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
      <h2>Sign Up</h2>
      <p>Please fill in this form to create an account!</p>
        <div class="form-group">
          <input type="text" class="form-control" name="username" placeholder="Username" required="required">
          <span class="help-block"><?php echo $username_err; ?></span>
        </div>
        <div class="form-group">
          <input type="password" class="form-control" name="password" placeholder="Password" required="required">
          <span class="help-block"><?php echo $password_err; ?></span>
        </div>
        <div class="form-group">
          <input type="password" class="form-control" name="confirm_password" placeholder="Confirm Password" required="required">
          <span class="help-block"><?php echo $confirm_password_err; ?></span>
        </div>
        <div class="form-group">
          <button type="submit" class="btn btn-outline-primary btn-lg">Sign Up</button>
        </div>
      </form>
      <div class="hint-text">Already have an account? <a href="/login">Login here</a>.</div>
    </div>
  </body>
</html>
