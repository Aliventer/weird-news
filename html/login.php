<?php
require_once "includes/functions.php";

session_start();
if (isset($_SESSION["loggedin"]) && $_SESSION["loggedin"] === true) {
    header("location: /");
    exit;
}

$username = trim($_POST["username"]);
$password = trim($_POST["password"]);
$username_err = $password_err = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
  $query = "SELECT id, password FROM users WHERE username = ?";
  if ($stmt = $conn -> prepare($query)) {
    $stmt -> bind_param("s", $param_username);

    $param_username = $username;

    if ($stmt -> execute()) {
      $stmt -> store_result();
      if ($stmt -> num_rows == 1) {
        $stmt -> bind_result($id, $hashed_password);
        if ($stmt -> fetch()) {
          if (password_verify($password, $hashed_password)) {
            session_start();
            $_SESSION["loggedin"] = true;
            $_SESSION["id"] = $id;
            $_SESSION["username"] = $username;

            header("location: /");
          } else {
            $password_err = "Invalid password.";
          }
        }
      } else {
        $username_err = "No account found with that username.";
      }
    }
    $stmt -> close();
  }
}
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <title>Weird News</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <style>
      .login-form {
        width: 390px;
        margin: 30px auto;
      }
    </style>
  </head>
  <body>
    <?php nav_menu(); ?>
    <div class="login-form">
      <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
      <h2>Login</h2>
      <p>Please fill in your credentials to login.</p>
        <div class="form-group">
          <input type="text" class="form-control" name="username" placeholder="Username" required="required">
          <span class="help-block"><?php echo $username_err; ?></span>
        </div>
        <div class="form-group">
          <input type="password" class="form-control" name="password" placeholder="Password" required="required">
          <span class="help-block"><?php echo $password_err; ?></span>
        </div>
        <div class="form-group">
          <button type="submit" class="btn btn-outline-primary btn-lg">Log in</button>
        </div>
      </form>
      <div class="hint-text">Don't have an account? <a href="/signup">Sign Up</a> now.</div>
    </div>
  </body>
</html>
