<?php
require_once "includes/functions.php";

session_start();
if (!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] === false) {
    header("location: /login");
    exit;
}

$query = "SELECT admin FROM users WHERE username = ?";
if ($stmt = $conn -> prepare($query)) {
  $stmt -> bind_param("s", $param_username);

  $param_username = $_SESSION["username"];

  if ($stmt -> execute()) {
    $stmt -> bind_result($is_admin);
    if ($stmt -> fetch()) {
      if ($is_admin == 0) {
        header("location: /");
        exit;
      }
    }
  }
  $stmt -> close();
}

$content = $id_err = "";

if ($_SERVER["REQUEST_METHOD"] == "GET") {
  $id = $_GET["id"];
  $query = "SELECT content FROM posts WHERE id = ?";
  if ($stmt = $conn -> prepare($query)) {
    $stmt -> bind_param("i", $param_id);

    $param_id = $id;

    if ($stmt -> execute()) {
      $stmt -> store_result();
      if ($stmt -> num_rows == 1) {
        $stmt -> bind_result($content);
        $stmt -> fetch();
      } else {
        $id_err = "This post doesn't exist.";
      }
    }
    $stmt -> close();
  }
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
  $content = $_POST["content"];
  $query = "UPDATE posts SET content = ? WHERE id = ?";
  if ($stmt = $conn -> prepare($query)) {
    if (strlen($content) < 255) {
      $stmt -> bind_param("si", $param_content, $param_id);

      $param_content = htmlspecialchars($content);
      $param_id = $_POST["id"];

      if ($stmt -> execute()) {
        header("location: /");
      }
    } else {
      $content_err = "Your post should be less than 255 characters long.";
    }
    $stmt -> close();
  }
}
?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Weird News</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <style>
    .content-form {
      width: 390px;
      margin: 30px auto;
    }
    </style>
  </head>
  <body>
    <?php nav_menu(); ?>
    <div class="content-form">
      <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
        <h2>Edit post</h2>
        <div class="form-group">
          <textarea class="form-control" name="content" rows="9" <?php echo $id_err == "" ? "required" : "readonly"; ?>><?php echo $content; ?></textarea>
          <input type="hidden" name="id" value=<?php echo $id; ?>>
          <span class="help-block"><?php echo $content_err; ?></span>
        </div>
        <?php
          if ($id_err != "") {
            echo "<div class=\"hint-text\">$id_err</div>";
          } else {
            echo "<div class=\"form-group\"><button type=\"submit\" class=\"btn btn-outline-primary btn-lg\">Submit</button></div>";
          }
        ?>
      </form>
    </div>
  </body>
</html>
