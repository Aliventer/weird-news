<?php
require_once "includes/functions.php";

session_start();
if (!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] === false) {
    header("location: /login");
    exit;
}

$content = $id_err = "";

if ($_SERVER["REQUEST_METHOD"] == "GET") {
  $id = $_GET["id"];
  $query = "DELETE FROM posts WHERE poster = ? AND id = ?";
  if ($stmt = $conn -> prepare($query)) {
    $stmt -> bind_param("si", $param_poster, $param_id);

    $param_poster = $_SESSION["username"];
    $param_id = $id;

    $stmt -> execute();
    $stmt -> close();
  }
  $query = "DELETE FROM comments WHERE post_id = ?";
  if ($stmt = $conn -> prepare($query)) {
    $stmt -> bind_param("i", $param_id);

    $param_id = $id;

    if ($stmt -> execute()) {
      header("location: /");
    }
    $stmt -> close();
  }
}
?>
