<?php
require_once "includes/functions.php";

session_start();
if (!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] === false) {
    header("location: /login");
    exit;
}

if ($_SERVER["REQUEST_METHOD"] == "GET") {
  $search = $_GET["search"];
  $query = "SELECT id, poster, content FROM posts WHERE content LIKE ? ORDER BY created_at DESC";
  if ($stmt = $conn -> prepare($query)) {
    $param_search = "%" . ($_GET["search"] == null ? "" : $_GET["search"]) . "%";
    $stmt -> bind_param("s", $param_search);

    if ($stmt -> execute()) {
      $stmt -> bind_result($id, $poster, $content);
      $inner = "<hr>";
      while ($stmt -> fetch()) {
        $inner .= "<textarea class=\"form-control\" name=\"content\" rows=\"3\" readonly>" . $content . "</textarea>";
        $inner .= "<span class=\"help-block\">" . "By: " . $poster . "." . " <a href=\"comment?id=$id\">Comment</a>" . ($poster == $_SESSION["username"] ? " | <a href=\"edit?id=$id\">Edit</a> | <a href=\"delete?id=$id\">Delete</a>" : "") . "</span><hr>";
      }
    }
    $stmt -> close();
  }
}
?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Weird News</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <style>
    .content-form {
      width: 390px;
      margin: 30px auto;
    }
    </style>
  </head>
  <body>
    <?php nav_menu(); ?>
    <div class="content-form">
      <h2>All posts</h2>
      <p>Want to write your own post? You can do it <a href="/post">here</a>.</p>
      <form class="form-inline" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="get">
        <div class="form-group">
          <input type="search" name="search" class="form-control" placeholder="Search posts">
          <button class="btn btn-outline-primary" type="submit"><i class="fa fa-search"></i></button>
        </div>
      </form>
      <div class="form-group">
        <?php echo $inner; ?>
      </div>
    </div>
  </body>
</html>
