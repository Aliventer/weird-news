### Requirements
- docker
- docker-compose
### How to run
```
git clone https://gitlab.com/Aliventer/weird-news.git && cd weird-news
docker-compose up [-d]
```
After that, go to your browser and type `localhost` in the url field.
